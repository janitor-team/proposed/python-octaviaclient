python-octaviaclient (3.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:40:11 +0200

python-octaviaclient (3.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Sep 2022 15:00:16 +0200

python-octaviaclient (3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Sep 2022 23:15:44 +0200

python-octaviaclient (2.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 11:27:41 +0100

python-octaviaclient (2.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Feb 2022 09:35:58 +0100

python-octaviaclient (2.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:50:29 +0200

python-octaviaclient (2.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Sep 2021 09:54:31 +0200

python-octaviaclient (2.3.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 11:37:12 +0200

python-octaviaclient (2.3.1-1) experimental; urgency=medium

  * New upstream release.

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 15 Jul 2021 12:45:52 +0200

python-octaviaclient (2.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Mar 2021 16:44:30 +0100

python-octaviaclient (2.2.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 16:46:19 +0200

python-octaviaclient (2.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switch to debhelper-compat = 11.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 10:12:58 +0200

python-octaviaclient (2.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Sep 2020 12:27:36 +0200

python-octaviaclient (2.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 23:28:36 +0200

python-octaviaclient (2.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Refreshed remove-privacy-breach.patch.
  * Blacklist 6 unit tests broken with Python 3.8.

 -- Thomas Goirand <zigo@debian.org>  Fri, 10 Apr 2020 09:34:36 +0200

python-octaviaclient (1.10.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:42:36 +0200

python-octaviaclient (1.10.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Update (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 08:46:48 +0200

python-octaviaclient (1.8.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 14:13:02 +0200

python-octaviaclient (1.8.0-2) experimental; urgency=medium

  * Fixed min version of python3-cliff.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Apr 2019 12:22:03 +0200

python-octaviaclient (1.8.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 20:34:02 +0100

python-octaviaclient (1.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 00:30:38 +0200

python-octaviaclient (1.6.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Building doc with Python 3.
  * Rebased remove-privacy-breach.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 12:21:12 +0200

python-octaviaclient (1.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:54:47 +0000

python-octaviaclient (1.4.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https in Format

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 21:17:34 +0000

python-octaviaclient (1.1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #877693)

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2017 13:53:21 +0200
